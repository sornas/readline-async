use readline_async::Editor;

use color_eyre::eyre::Result;
use tokio::time::{sleep, Duration};

#[tokio::main]
async fn main() -> Result<()> {
    let (mut editor, lines) = Editor::new();
    readline_async::enable_raw_mode()?;

    loop {
        match editor.readline().await {
            Err((e, _)) => {
                readline_async::disable_raw_mode()?;
                println!("\n{:?}", e);
                return Ok(());
            }
            Ok(line) => {
                lines.unbounded_send(format!(">> {}", line))?;
                let lines = lines.clone();
                tokio::task::spawn(async move {
                    sleep(Duration::from_millis(1000)).await;
                    lines.unbounded_send(format!("echo: {}", line)).unwrap();
                });
            }
        }
    }
}
